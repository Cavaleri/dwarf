﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dwarfs
{
    enum Player { One, Two }

    class Dwarf
    {
        public Vector2 Position;
        public Texture2D Sprite;
        float speed;
        float currentSpeed;
        Player player;
        float rotation;
        float rotationSpeed;
        public float Radius;

        public Dwarf(Vector2 position, Texture2D sprite, Player player)
        {
            this.Position = position;
            this.speed = 200f;
            this.currentSpeed = 0f;
            this.Sprite = sprite;
            this.player = player;

            this.rotation =(float) (90f * Math.PI / 180);
            this.rotationSpeed = 10f;
            this.Radius = this.Sprite.Width / 2;
        }

        public void Update(GameTime gameTime)
        {
            // reset speed
            this.currentSpeed = 0f;

            if (Keyboard.GetState().IsKeyDown(Keys.W) && player == Player.One || Keyboard.GetState().IsKeyDown(Keys.Up) && player == Player.Two)
            {
                currentSpeed = -(speed);

            }
            else if (Keyboard.GetState().IsKeyDown(Keys.S) && player == Player.One || Keyboard.GetState().IsKeyDown(Keys.Down) && player == Player.Two)
            {
                currentSpeed = speed;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.A) && player == Player.One || Keyboard.GetState().IsKeyDown(Keys.Left) && player == Player.Two)
            {
                rotation -= (float)(rotationSpeed * gameTime.ElapsedGameTime.TotalSeconds);

            }
            else if (Keyboard.GetState().IsKeyDown(Keys.D) && player == Player.One || Keyboard.GetState().IsKeyDown(Keys.Right) && player == Player.Two)
            {
                rotation += (float)(rotationSpeed * gameTime.ElapsedGameTime.TotalSeconds);
            }
            
            Vector2 direction = new Vector2((float)Math.Cos(rotation), (float)Math.Sin(rotation));

            float speedOverTime = (float)(currentSpeed * gameTime.ElapsedGameTime.TotalSeconds);
            this.Position += direction * speedOverTime;
        }

        public void Draw (SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Sprite, Position, null, Color.White, rotation, new Vector2(Sprite.Width/2, Sprite.Height/2), 1f, SpriteEffects.None, 1f);
        }


        public bool CheckDwarfCollision(Dwarf other)
        {
            float dis_x = this.Position.X - other.Position.X;
            float dis_y = this.Position.Y - other.Position.Y;
            double dis = Math.Sqrt(dis_x * dis_x + dis_y * dis_y);

            if (dis < Radius + other.Radius)
            {
                return true;
            }
            return false;
        }

        public void DwarfCollision(Dwarf other)
        {
            Vector2 normal = this.Position - other.Position;
            normal.Normalize();

            float distance = Vector2.Distance(Position, other.Position);
            float maxDistance = Radius + other.Radius;
            float moveBack = maxDistance - distance + 2f / 2f;

            this.Position += normal * moveBack;
            other.Position -= normal * moveBack;
            
        }
    }
}
