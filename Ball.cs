﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dwarfs
{
    class Ball
    {
        Vector2 position;
        Vector2 direction;
        public Texture2D Sprite;
        float speed;
        float incSpeed;
        float maxSpeed;
        public float Radius;
        float rotation;

        public Ball(Vector2 position, float maxSpeed, Texture2D sprite, float incSpeed)
        {
            this.position = position;
            this.direction = Vector2.Zero;
            this.speed = 0f;
            this.maxSpeed = maxSpeed;
            this.incSpeed = incSpeed;
            this.Sprite = sprite;
            this.Radius = Sprite.Width / 2;
            this.rotation = (float)(0f * Math.PI / 180);
        }

        public Vector2 Position { get => position; set => position = value; }
        public float Speed { get => speed; set => speed = value; }
        public float MaxSpeed { get => maxSpeed; set => maxSpeed = value; }
        public Vector2 Direction { get => direction; set => direction = value; }

        public void Update (GameTime gameTime)
        {
            this.position += (this.direction * (float)speed * (float)gameTime.ElapsedGameTime.TotalSeconds);

            this.speed *= 0.98f;
        }

        public void OnDwarfCollision(Dwarf dwarf)
        {
            Vector2 normal = this.Position - dwarf.Position;
            normal.Normalize();

            float distance = Vector2.Distance(Position, dwarf.Position);
            float maxDistance = Radius + dwarf.Radius;
            float moveBack = maxDistance - distance + 2f / 2f;

            this.Direction = normal;
            this.speed = ((this.speed + incSpeed) <= maxSpeed) ? this.speed + this.incSpeed : this.maxSpeed;

            this.Position += normal * moveBack;
            dwarf.Position -= normal * moveBack;
        }

        public bool CheckDwarfCollision(Dwarf dwarf)
        {
            float dis_x = this.Position.X - dwarf.Position.X;
            float dis_y = this.Position.Y - dwarf.Position.Y;
            double dis = Math.Sqrt(dis_x * dis_x + dis_y * dis_y);

            if (dis < Radius + dwarf.Radius)
            {
                return true;
            }
            return false;
        }


        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Sprite, Position, null, Color.White, rotation, new Vector2(Sprite.Width / 2, Sprite.Height / 2), 1f, SpriteEffects.None, 1f);
        }
    }
}
