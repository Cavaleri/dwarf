﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Dwarfs
{
    enum GameState { MainMenu, Hosting, Joining, Playing }

    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D dwarfTexture;
        Texture2D ballTexture;

        Dwarf playerOne;
        Dwarf playerTwo;
        Ball theBall;

        GameState currentGameState;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferHeight = 1080;
            graphics.PreferredBackBufferWidth = 1920;

            currentGameState = GameState.MainMenu;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            dwarfTexture = Content.Load<Texture2D>("dwarf");
            ballTexture = Content.Load<Texture2D>("ball");

            playerOne = new Dwarf(new Vector2(250f, 250f), dwarfTexture, Player.One);
            playerTwo = new Dwarf(new Vector2(550f, 450f), dwarfTexture, Player.Two);
            theBall = new Ball(new Vector2(400f, 325f), 400f, ballTexture, 250f);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            playerOne.Update(gameTime);
            playerTwo.Update(gameTime);
            theBall.Update(gameTime);

            if (playerOne.CheckDwarfCollision(playerTwo))
            {
                playerOne.DwarfCollision(playerTwo);

            }
            if (playerTwo.CheckDwarfCollision(playerOne))
            {
                playerTwo.DwarfCollision(playerOne);

            }

            // ball collision
            if (theBall.CheckDwarfCollision(playerOne))
            {
                theBall.OnDwarfCollision(playerOne);
            }
            else if (theBall.CheckDwarfCollision(playerTwo))
            {
                theBall.OnDwarfCollision(playerTwo);
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            playerOne.Draw(spriteBatch);
            playerTwo.Draw(spriteBatch);
            theBall.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
